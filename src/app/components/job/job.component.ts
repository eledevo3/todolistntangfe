import { StatusService } from './../../Service/statusService/status.service';
import { Component, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subject, takeUntil } from 'rxjs';
import { IJob, IPayloadJob, IStateGetJob } from '../../Interface/IJob';
import { limit } from '../../constantCommon/constantCommon';
import { priorityFormat } from '../../utils/priorityFormat';
import { JobService } from '../../Service/jobService/job.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrl: './job.component.css'
})
export class JobComponent {
  constructor(private jobService: JobService, private notification: NzNotificationService) { }
  item: IJob = {
    idJob: 0,
    nameJob: '',
    idStatus: 0,
    nameStatus: '',
    priority: 'ONE',
    progress: '0',
    progressDouble: 0,
    hasChild: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    idManage: 0
  };

  nameJob: string = ''
  isVisible = false;
  isLoading = false;
  isSearch: boolean = false;
  listData: IJob[] = [];
  limit = limit;
  totalRecord: number = 0
  modalDeleteItem: IJob | any = {}

  modalJob = {
    modalJobItem: false,
    modalAddorUpdate: false,
    modalDelete: false
  }

  titleJob: string = "";
  sortItem = 9
  idManage: number | null = 0;
  idJob: number = 0;
  priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE' = 'ONE';
  nameStatus: string | undefined = "";
  idStatus: number | undefined = 0;
  showValidate: boolean = false

  private destroyed$ = new Subject()
  stateGet = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }

  modalItemAddorUpdate = {
    listData: {} as IJob[] | any,
    index: 0 as number | any,
    itemData: {} as IJob | any
  }

  modalJobItem: IJob | any = {}
  modalItemDelete: IJob | any = {}


  handleCloseModalDelete() {
    this.modalJob.modalDelete = false;
  }

  handleOpenModalDelete(data: IJob) {
    this.jobService.checkJob(data.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", '', 'Công việc không tồn tại');
          this.jobService.refreshListData(this.stateGet).subscribe({
            next: (v) => {
              if (v.status === false) {
                this.notification.create("warning", `${v.content.warning}`, '');
              }
              this.listData = v.content.list
              this.totalRecord = v.content.totalRecord
              this.stateGet.currentPage = v.content.currentPage
              this.isLoading = false
            },
            error: (e) => {
              this.notification.create("error", `${e.message}`, '');
              this.isLoading = false
            }
          })
        }
        else {
          this.modalJob.modalDelete = true;
          this.modalDeleteItem = data
          this.nameJob = data.nameJob;
          this.item.idJob = data.idJob
        }
      },
      error: (e) => {
        this.notification.create("warning", "Công việc không tồn tại", '')
        this.notification.create("error", `${e.message}`, '');
      }
    })
  }

  handleSubmitDelete(): void {
    this.isLoading = true;
    this.jobService.delete(this.item.idJob, this.stateGet.currentPage, limit).subscribe({
      next: (v) => {
        if (v.status === true) {
          this.notification.create("success", `${v.message}`, '')
          if (this.idManage == null || this.idManage == 0) {
            this.jobService.refreshListData(this.stateGet).subscribe({
              next: (v) => {
                if (v.status === true) {
                  if (v.content.warning != "") {
                    this.notification.create("warning", `${v.content.warning}`, '')
                  }
                } else {
                  this.notification.create('warning', `${v.content.warning}`, '')
                  this.notification.create("error", `${v.message}`, '');
                }
                this.modalJob.modalDelete = false
              },
              error: (error) => {
                this.notification.create("error", `${error.message}`, '');
              }
            })
          } else {
            this.jobService.getJobById(this.stateGet, this.idManage).subscribe({
              next: (v) => {
                if (v.status === false) {
                  this.notification.create("warning", `${v.message}`, '')
                }
              },
              error: (error) => {
                this.notification.create("error", `${error.message}`, '');
              }
            })
          }
        }
        else {
          this.notification.create("warning", `${v.content.warning}`, ""),
            this.notification.create("error", `${v.message}`, "")
        }
      }, error: (e) => {
        this.notification.create('error', `${e.message}`, '');
      }
    }
    )
  }
  handleGetAll() {
    this.isLoading = true
    this.jobService.refreshListData(this.stateGet).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("error", `${v.message}`, '');
        }
        this.isLoading = false
      },
      error: (error) => {
        this.notification.create(
          "error", `${error.message}`, '');
        this.isLoading = false
      }
    })
  }
  ngOnInit(): void {
    this.handleGetAll();
    this.jobService.get().pipe(takeUntil(this.destroyed$)).subscribe(data => {
      this.listData = data.content.list
      this.totalRecord = data.content.totalRecord
      this.stateGet.currentPage = data.content.currentPage
    })
  }


  handleOpenModalAddorUpdate(data?: IJob, index?: number) {
    if(data) {
      this.jobService.checkJob(data.idJob).subscribe({
        next: (v) => {
          if (v.status === true) {
            this.titleJob = "Sửa công việc"
            this.modalItemAddorUpdate.listData = this.listData
            this.modalItemAddorUpdate.itemData = data
            this.modalItemAddorUpdate.index = index
            this.modalJob.modalAddorUpdate = true
          }
          else {
            this.notification.create('error', `${v.message}`, '');
            this.handleGetAll();
          }

        },
        error: (e) => {
          this.notification.create('error', `${e.message}`, '');
        }
      })
    } else {
      this.titleJob = "Thêm công việc"
      this.modalItemAddorUpdate.itemData = {
        idJob: 0,
        nameJob: '',
        priority: 'ONE',
        idStatus: 2,
        nameStatus: 'Chưa hoàn thành',
        createdAt: new Date,
        updatedAt: new Date
      }
      this.modalJob.modalAddorUpdate = true;
    }

  }
  handleCloseModalAddorUpdate() {
    this.modalJob.modalAddorUpdate = false;
  }
  onInput() {
    this.showValidate = this.nameJob.trim() === '';
  }

  handleAddOrUpdate(data : Omit<IPayloadJob, "idJob, idManage">): void{
    this.nameJob = data.nameJob;
    this.idStatus = data.idStatus;
    this.nameStatus = data.nameStatus;
    this.priority = data.priority
    if(data.nameJob){
      this.isLoading = true
      if(this.modalItemAddorUpdate.itemData.idJob){
        this.jobService.update(this.modalItemAddorUpdate.itemData.idJob, data.nameJob, data.priority, data.idStatus, data.nameStatus )
        .subscribe({
          next: (v)=>{
            if (v.status == true) {
              if (v.content.warning == "") {
                this.notification.create('success', `${v.message}`,'')
              } else {
                this.notification.create('warning', `${v.content.warning}`,'')
                this.notification.create('success', `${v.message}`,'')
              }
              const updatedListData = this.modalItemAddorUpdate.listData
              if(this.modalItemAddorUpdate.itemData.hasChild === true){
                const itemJobUpdate: IPayloadJob = {
                  idJob : this.modalItemAddorUpdate.itemData.idJob,
                  nameJob: data.nameJob,
                  idStatus: data.idStatus,
         
                  priority: data.priority,
                  updatedAt: this.modalItemAddorUpdate.itemData.updatedAt,
                  createdAt: this.modalItemAddorUpdate.itemData.createdAt,
                  progress: this.modalItemAddorUpdate.itemData.progress
                }
              }else{
                const itemJobUpdate: IPayloadJob = {
                  idJob : this.modalItemAddorUpdate.itemData.idJob,
                  nameJob: data.nameJob,
                  nameStatus: data.nameStatus,
                  idStatus: data.idStatus,
                  priority: data.priority,
                  updatedAt: this.modalItemAddorUpdate.itemData.updatedAt,
                  createdAt: this.modalItemAddorUpdate.itemData.createdAt,
                }
                if(this.modalItemAddorUpdate.index === undefined){
                  updatedListData[0] = itemJobUpdate
                }else{
                  updatedListData[this.modalItemAddorUpdate.index] = itemJobUpdate
                }
                this.listData = updatedListData
              }
            }else{
                this.notification.create('warning','' ,'Trạng thái không tồn tại')
                this.jobService.refreshListData(this.stateGet).subscribe({
                  next:(v) =>{
                    if(v.status === false){
                      this.notification.create('warning',`${v.message}`,'' );
                  }
                  this.listData = v.content.list
                },
                error:(error) =>{}
                })
            }
            this.isLoading = false
            this.modalJob.modalAddorUpdate = false;
            this.handleResetState()
          },
          error: (error)=>{
            this.notification.create('error', `${error.message}`,'');
          }
        })
      }else{
        this.jobService.add(this.modalItemAddorUpdate.itemData.idManage, data.nameJob, data.priority, data.idStatus, data.nameStatus)
        .subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create("error", `${v.message}`, '');
              this.notification.create("warning",`${v.content.warning}`, '');
            } else {
              this.notification.create("success", `${v.message}`, '')
            }
              this.handleResetState()
              this.jobService.refreshListData(this.stateGet).subscribe({
                next: (v) => {
                  if (v.status === true) {
                    if (v.content.warning != "") {
                      this.notification.create("warning", `${v.content.warning}`, '')
                    }
                  }else{
                    this.notification.create('warning', `${v.content.warning}`,'')
                    this.notification.create("error", `${v.message}`, '');
                  }
                },
                error: (error) => {
                  this.notification.create("error", `${error.message}`, '');
                }
              })
            this.isLoading = false;
            this.modalJob.modalAddorUpdate = false
          },
          error: (error) =>{
            this.notification.create("error", `${error.message}`, '');
          }
        })
      }
    }else{
      this.onInput()
    }
  }
  handleResetState() {
    this.idManage = 0;
    this.nameJob = '';
    this.priority = 'ONE';
    this.idStatus = 1;
  }

  handleOpenModalJob(data: IJob) {
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }

    this.modalJobItem = data
    this.jobService.getJobById(this.stateGet, this.modalJobItem.idJob).subscribe({
      next: (v) => {
        if (v.status == false) {
          this.notification.create(
            "warning", `${v.content.warning}`, '');
          this.isLoading = false
          this.jobService.refreshListData(this.stateGet)
        }
        else {
          this.modalJob.modalJobItem = true
          this.listData = v.content.list
          this.totalRecord = v.content.totalRecord
          this.stateGet.currentPage = v.content.currentPage
        }
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
        this.isLoading = false
      }
    })
  }

  handleCloseModalJob() {
    this.modalJob.modalJobItem = false

    this.isLoading = true
    this.modalJobItem = {
      idJob: 0,
      nameJob: '',
      idStatus: 0,
      nameStatus: '',
      priority: 'ONE',
      progress: '',
      progressDouble: 1,
      hasChild: false,
      createdAt: new Date(),
      updatedAt: new Date(),
      idManage: 0
    }
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }
    this.jobService.refreshListData(this.stateGet).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
        }
        this.isLoading = false
      },
      error: (error) => {
        this.notification.create(
          "error", `${error.message}`, '');
        this.isLoading = false
      }
    })
  }

  handleReSelectSortType(value: number) {
    this.sortItem = value;
  }

  handleSearch(event: IStateGetJob): void {
    this.isSearch = true;
    this.stateGet.currentPage = 1
    this.jobService.refreshListData(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handlePagination(event: number) {
    if (!this.isSearch) {
      this.stateGet.textSearch = '';
      this.isLoading = true;
      this.stateGet.currentPage = event;

      this.jobService.refreshListData(this.stateGet).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.listData = v.content.list
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    } else {
      this.isLoading = true;
      this.stateGet.currentPage = event;

      this.jobService.refreshListData(this.stateGet).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.listData = v.content.list
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    }
  }
  handlePriorityFormat(priority: string) {
    return priorityFormat(priority)
  }
}


