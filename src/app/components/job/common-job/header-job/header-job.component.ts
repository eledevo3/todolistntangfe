import { Component, EventEmitter, Input, Output } from '@angular/core';
import { sortCase, sortCaseFilter } from '../../../../utils/jobSort';
import { JobService } from '../../../../Service/jobService/job.service';
import { IStateGetJob } from '../../../../Interface/IJob';

@Component({
  selector: 'app-header-job',
  templateUrl: './header-job.component.html',
  styleUrl: './header-job.component.css'
})
export class HeaderJobComponent {
  constructor(private jobService: JobService) { }
  @Input() stateGet = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }
  @Output() searchJob = new EventEmitter<IStateGetJob>;
  @Output() openModalAdd: EventEmitter<void> = new EventEmitter<void>();


  sortCase = sortCase
  sortItem = 9

  inputValue() {
    this.stateGet.textSearch = "";
    this.searchJob.emit(this.stateGet);
  }

  handleOpenModalAdd() {
    this.openModalAdd.emit();
  }

  handleSelectData(e: number) {
    const data = sortCaseFilter(e)
    if (typeof (data) === 'number') {
      return
    }
    this.stateGet.sortType = data.sortType
    this.stateGet.sortData = data.sortData
    this.stateGet.currentPage = 1
    this.jobService.refreshListData(this.stateGet).subscribe();
  }

  handleSearch() {
    this.searchJob.emit(this.stateGet);
  }


}
