import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddOrUpdateJobComponent } from './modal-add-or-update-job.component';

describe('ModalAddOrUpdateJobComponent', () => {
  let component: ModalAddOrUpdateJobComponent;
  let fixture: ComponentFixture<ModalAddOrUpdateJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalAddOrUpdateJobComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalAddOrUpdateJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
