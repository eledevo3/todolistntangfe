import { Component, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { IPayloadJob, IStateGetJob } from '../../../../Interface/IJob';
import { IJob } from '../../../../Interface/IJob';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StatusService } from '../../../../Service/statusService/status.service';
import { JobService } from '../../../../Service/jobService/job.service';

@Component({
  selector: 'app-modal-add-or-update-job',
  templateUrl: './modal-add-or-update-job.component.html',
  styleUrl: './modal-add-or-update-job.component.css'
})
export class ModalAddOrUpdateJobComponent {
  constructor(private statusService: StatusService, private notification: NzNotificationService) {
    this.handleGetAllStatus()
   }
  @Input() item: {
    listData: IJob[],
    itemData: IJob,
    index: 0
  } = {
      listData: [],
      itemData: {
        idJob: 0,
        nameJob: '',
        idStatus: 0,
        nameStatus: '',
        priority: 'ONE',
        progress: '0',
        progressDouble: 0,
        hasChild: false,
        createdAt: new Date(),
        updatedAt: new Date(),
        idManage: 0,
      },
      index: 0
    }
    @Input() titleJob: string = '';
    @Input() isVisible : boolean = false;
    @Output() handleImpl :EventEmitter<Omit<IPayloadJob, "idJob, idManage">> = new EventEmitter<Omit<IPayloadJob, "idJob, idManage">>();
    @Output() closeModal :EventEmitter<void> = new EventEmitter<void>();
    @Output() searchStatus :EventEmitter<any> = new EventEmitter<any>();


  idJob: number = 0;
  nameJob: string = '';
  priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE' = 'ONE';
  nameStatus: string = "";
  idStatus: number = 1;
  idManage: number = 0;
  title: string = '';
  showValidate: boolean = false;
  isLoadingSelect: boolean = false
  optionStatus: any = [];

    itemJob: Omit<IPayloadJob, "idJob, idManage">={
      nameJob: "",
      priority: 'ONE',
      nameStatus: "",
      idStatus: 1,
    }

    ngOnChanges(changes: SimpleChanges) : void{
      if(changes['item']&&changes['item'].currentValue.itemData.idJob > 0){
        this.title = this.titleJob;
        this.idJob = this.item.itemData.idJob;
        this.nameJob = this.item.itemData.nameJob
        this.priority = this.item.itemData.priority
        this.idStatus = this.item.itemData.idStatus
        this.nameStatus = this.item.itemData.nameStatus
      }else if(this.item.itemData.idJob > 0){
        this.title = this.titleJob;
        this.idJob = this.item.itemData.idJob;
        this.nameJob = this.item.itemData.nameJob
        this.priority = this.item.itemData.priority
        this.idStatus = this.item.itemData.idStatus
        this.nameStatus = this.item.itemData.nameStatus
        this.handleGetAllStatus()
      }else{
        this.title = this.titleJob;
        this.nameJob = this.item.itemData.nameJob
        this.priority = this.item.itemData.priority
        this.idStatus = this.item.itemData.idStatus
        this.nameStatus = this.item.itemData.nameStatus
      }
    }
    handleResetState() {
      this.idJob = 0;
      this.nameJob = '';
      this.priority = 'ONE';
      this.idStatus = 1;
    }
    onInput() {
      this.showValidate = this.nameJob.trim() === '';
    }

    handleCancel() {
      this.closeModal.emit();
      this.handleResetState();
      this.showValidate = false;
    }
    handleOk(): void{
      this.itemJob.nameJob = this.nameJob
      this.itemJob.idStatus = this.idStatus
      this.itemJob.priority = this.priority
      this.itemJob.nameStatus = this.nameStatus
      this.itemJob.idManage = this.item.itemData.idManage
      if (this.itemJob.nameJob.trim() === '') {
      this.onInput()
      }else{
        this.handleImpl.emit(this.itemJob);
      }
    }
    handleGetAllStatus(){
      this.statusService.getAll().subscribe({
        next: (v) => {
          this.optionStatus = v.content.list.map((item: any) => ({
            label: item.nameStatus,
            value: item.idStatus,
          }));
          this.isLoadingSelect = false
        }, error: (e) => {
          this.notification.create(
            "error", `${e.message}`, '');
          this.isLoadingSelect = false
        }
      }
      );
    }
    onChangeNameStatus(idStatus: number) {
      let result = this.optionStatus.find((item : any) =>  item.value == idStatus);
      this.nameStatus = result.label
    }

}
