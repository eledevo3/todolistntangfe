import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteJobComponent } from './modal-delete-job.component';

describe('ModalDeleteJobComponent', () => {
  let component: ModalDeleteJobComponent;
  let fixture: ComponentFixture<ModalDeleteJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalDeleteJobComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalDeleteJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
