import { Component, EventEmitter, Input, Output } from '@angular/core';
import { JobService } from '../../../../Service/jobService/job.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { IJob, IStateGetJob } from '../../../../Interface/IJob';
@Component({
  selector: 'app-modal-delete-job',
  templateUrl: './modal-delete-job.component.html',
  styleUrl: './modal-delete-job.component.css'
})
export class ModalDeleteJobComponent {
  constructor(private notification: NzNotificationService,
    private jobService: JobService) { }
    @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
    @Output() deleteItem: EventEmitter<void> = new EventEmitter<void>();
    @Input() isVisibleDelete: boolean = false;
    @Input() nameJob: string =''
    @Input() stateGet: IStateGetJob = {
      textSearch: '',
      sortData: 'updatedAt',
      sortType: 'DESC',
      currentPage: 1
    };
    @Input() item: IJob = {
      idJob: 0,
      nameJob: '',
      idStatus: 0,
      nameStatus: '',
      priority: 'ONE',
      progress: '0',
      progressDouble: 0,
      hasChild: false,
      createdAt: new Date(),
      updatedAt: new Date(),
      idManage: 0
    };
    isLoadingDelete: boolean = false;
    listData: IJob[] = []
    totalRecord = 0;
    
    handleOk(): void {
      this.deleteItem.emit();
    }
    handleCancel(): void {
      this.closeModal.emit();
    }
}
