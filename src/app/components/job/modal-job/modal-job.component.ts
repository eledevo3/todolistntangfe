import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { IJob, IPayloadJob, IStateGetJob } from '../../../Interface/IJob';
import { priorityFormat } from '../../../utils/priorityFormat';
import { limit } from '../../../constantCommon/constantCommon';
import { JobService } from '../../../Service/jobService/job.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subject, takeUntil } from 'rxjs';
import { sortCase, sortCaseFilter } from '../../../utils/jobSort';
import { StatusService } from '../../../Service/statusService/status.service';

@Component({
  selector: 'app-modal-job',
  templateUrl: './modal-job.component.html',
  styleUrl: './modal-job.component.css'
})

export class ModalJobComponent {
  // openModalAdd: any;
  constructor(
    private jobService: JobService,
    private notification: NzNotificationService,
    private statusSevice: StatusService
  ) { }

  private destroyed$ = new Subject()

  isVisible = false;
  isLoadingSelect: boolean = false
  isLoadingConfirm: boolean = false
  showValidate: boolean = false;
  options: any = [];

  title: string = '';
  idJob: number = 0;
  nameJob: string = '';
  priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE' = 'ONE';
  idStatus: number | undefined;
  nameStatus: string | undefined = ''
  idManage: number = 0;

  @Input() isVisibleModal: boolean = false
  @Input() item: IJob = {
    idJob: 0,
    nameJob: '',
    idStatus: 0,
    nameStatus: '',
    priority: 'ONE',
    progress: '0',
    progressDouble: 0,
    hasChild: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    idManage: 0
  }
  @Input() isLoading: boolean = false

  @Input() stateGet = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }

  @Input() stateGetModalJob = {
    textSearch: '',
    sortType: 'desc',
    sortData: 'updatedAt',
    currentPage: 1
  }
  
  @Output() searchJob = new EventEmitter<IStateGetJob>;
  @Output() openModalAdd: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();

  listDatas: IJob[] = []
  listData: IJob[] = []
  limit = limit;
  sortCase = sortCase
  sortItem = 9
  totalRecord = 0;
  stateGetById = {}
  isSearch: boolean = false;

  // open modal
  modalJob = {
    modalJobItem: false,
    modalAddorUpdate: false,
    modalDelete: false
  }

  modalItemAddorUpdate = {
    listData: {} as IJob[] | any,
    index: 0 as number | any,
    itemData: {} as IJob | any
  }

  modalDeleteItem: IJob | any = {}
  modalJobItem: IJob = {
    idJob: 0,
    nameJob: "",
    idStatus: 1,
    nameStatus: "" ,
    priority: 'ONE',
    progress: "",
    progressDouble: 0,
    hasChild: false,
    createdAt: new Date,
    updatedAt: new Date,
    idManage: 0
  }
  modalItemDelete: IJob | any = {}

  itemJob: Omit<IPayloadJob, "idJob"> = {
    nameJob: "",
    priority: 'ONE',
    nameStatus: "",
    idStatus: 1,
    idManage: 0
  }

  handleOpenModalAddorUpdate(data?: IJob, index?: number) {
    if (data) {
      this.jobService.checkJob(data.idJob).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create("warning", `${v.message}`, '');
            this.jobService.getJobById(this.stateGet, this.breadcrumbItems[this.breadcrumbItems.length-1].idJob).subscribe({
              next: (v)=>{
                if(v.status === false){
                  this.notification.create("warning", `${v.content.warning}`, "")
                  this.closeModal.emit()
                  this.jobService.refreshListData(this.stateGet).subscribe({
                    next: (v)=>{
                      if(v.status === false){
                        this.notification.create("warning", `${v.content.warning}`, "")
                      }else{
                        this.listData = v.content.list
                        this.totalRecord = v.content.totalRecord
                        this.stateGet.currentPage = v.content.currentPage
                      }
                    },
                    error: (e) => {
                      this.notification.create(
                        "error", `${e.message}`, '');
                    }
                  })
                }else{
                  this.breadcrumbItems = this.breadcrumbItems.slice(0, this.breadcrumbItems.length);
                  this.listData = v.content.list
                  this.totalRecord = v.content.totalRecord
                  this.stateGet.currentPage = v.content.currentPage
                }
              },
              error: (e) => {
                this.notification.create(
                  "error", `${e.message}`, '');
              }
            })


          }else{
            this.modalItemAddorUpdate.listData = this.listData
            this.modalItemAddorUpdate.itemData = data
            this.modalItemAddorUpdate.index = index
            this.modalJob.modalAddorUpdate = true
          }
        },
        error: (e) => {
          this.notification.create(
            "error", `${e.message}`, '');
        }
      })
    } else {
      this.jobService.checkJob(this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob).subscribe({
        next: (v)=>{
          if(v.status === false){
            this.notification.create('warning', `${v.message}`,"")
            this.breadcrumbItems = this.breadcrumbItems.slice(0, this.breadcrumbItems.length -1);
            if(this.breadcrumbItems.length -1 > 0){
              this.jobService.getJobById(this.stateGet, this.breadcrumbItems[this.breadcrumbItems.length-1].idJob).subscribe({
                next: (v)=>{
                  if(v.status === false){
                    this.notification.create("warning", `${v.content.warning}`, "")
                    this.closeModal.emit()
                    this.jobService.refreshListData(this.stateGet).subscribe({
                      next: (v)=>{
                        if(v.status === false){
                          this.notification.create("warning", `${v.content.warning}`, "")
                        }else{
                          this.listData = v.content.list
                          this.totalRecord = v.content.totalRecord
                          this.stateGet.currentPage = v.content.currentPage
                        }
                      },
                      error: (e) => {
                        this.notification.create(
                          "error", `${e.message}`, '');
                      }
                    })
                  }else{
                    this.breadcrumbItems = this.breadcrumbItems.slice(0, this.breadcrumbItems.length);
                    this.listData = v.content.list
                    this.totalRecord = v.content.totalRecord
                    this.stateGet.currentPage = v.content.currentPage
                  }
                },
                error: (e) => {
                  this.notification.create(
                    "error", `${e.message}`, '');
                }
              })
            }else{
              this.closeModal.emit()
              this.jobService.refreshListData(this.stateGet).subscribe({
                next: (v)=>{
                  if(v.status === false){
                    this.notification.create("warning", `${v.content.warning}`, "")
                  }else{
                    this.listData = v.content.list
                    this.totalRecord = v.content.totalRecord
                    this.stateGet.currentPage = v.content.currentPage
                  }
                },
                error: (e) => {
                  this.notification.create(
                    "error", `${e.message}`, '');
                }
              })
            }
          }else{
            this.modalJob.modalAddorUpdate = true;
            this.title = "Thêm Công Việc"
            this.modalItemAddorUpdate.itemData = {
            idJob: 0,
            nameJob: '',
            idStatus: 2,
            priority: 'ONE',
            nameStatus: 'Chưa hoàn thành',
            }
          }
        }
      })
    }
  }

  // xử lý handleOk add or update job
  handleImpl(data: Omit<IPayloadJob, "idJob, idManage">) {
    this.nameJob = data.nameJob;
    this.idStatus = data.idStatus;
    this.nameStatus = data.nameStatus;
    this.priority = data.priority;
    this.idManage = this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob;
    if (data.nameJob) {
      this.isLoading = true
      if (this.modalItemAddorUpdate.itemData.idJob) {
        this.jobService.update(this.modalItemAddorUpdate.itemData.idJob, data.nameJob, data.priority, data.idStatus, data.nameStatus)
          .subscribe({
            next: (v) => {
              if (v.status === true) {
                if (v.content.warning != "") {
                  this.notification.create("warning", `${v.content.warning}`, '')
                }
                this.notification.create("success", `${v.message}`, '')
                this.modalJob.modalAddorUpdate = false
                const updatedListData = this.modalItemAddorUpdate.listData
                const itemJobUpdate: IPayloadJob = {
                  idJob: this.modalItemAddorUpdate.itemData.idJob,
                  nameJob: data.nameJob,
                  nameStatus: data.nameStatus,
                  idStatus: data.idStatus,
                  priority: data.priority,
                  updatedAt: this.modalItemAddorUpdate.itemData.updatedAt,
                  createdAt: this.modalItemAddorUpdate.itemData.createdAt
                }
                if (this.modalItemAddorUpdate.index === undefined) {
                  updatedListData[0] = itemJobUpdate
                } else {
                  updatedListData[this.modalItemAddorUpdate.index] = itemJobUpdate
                }
                this.listData = updatedListData
              } else {
                this.isLoading = false;
                this.stateGetById = this.stateGet;
                this.notification.create("error", `${v.message}`, '');
                this.jobService.checkJob(this.modalItemAddorUpdate.itemData.idManage).subscribe({
                  next: (v) => {
                    if (v.status === true) {
                      this.notification.create("success", `${v.message}`, '')
                      this.handleGetById(this.modalItemAddorUpdate.itemData.idManage)
                    } else {
                      this.notification.create("error", `${v.message}`, '');
                      this.closeModal.emit();
                      this.jobService.refreshListData(this.stateGet).subscribe({
                        next: (v) => {
                          if (v.status === true) {
                            this.notification.create("success", `${v.message}`, '')
                          } else {
                            this.notification.create("error", `${v.message}`, '');
                          }
                        },
                        error: (error) => {
                          this.notification.create("error", `${error.message}`, '');
                        }
                      })
                    }
                  },
                  error: (error) => {
                    this.notification.create("error", `${error.message}`, '');
                  }
                })
              }
            },
            error: (error) => {
              this.notification.create("error", `${error.message}`, '');
            }
          })
      } else {
        //add
        this.itemJob = data
        this.itemJob.idManage = this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob
        this.jobService.add(this.itemJob.idManage, this.itemJob.nameJob, this.itemJob.priority, this.itemJob.idStatus, this.itemJob.nameStatus)
          .subscribe({
            next: (v) => {
              if (v.status === true) {
                this.notification.create("success", `${v.message}`, '')
                this.modalJob.modalAddorUpdate = false
                this.handleGetById(this.breadcrumbItems[this.breadcrumbItems.length - 1])
              } else {
                this.jobService.checkJob(this.modalItemAddorUpdate.itemData.idManage).subscribe({
                  next: (v) => {
                    if (v.status === false) {
                      this.notification.create("warning", `${v.message}`, '');
                      this.modalJob.modalAddorUpdate = false
                      this.closeModal.emit();
                      this.jobService.refreshListData(this.stateGet).subscribe({
                        next: (v) => {
                          if (v.status === false) {
                            this.notification.create("error", `${v.message}`, '');
                          }
                          this.listData = v.content.list
                          this.totalRecord = v.content.totalRecord
                          this.stateGet.currentPage = v.content.currentPage
                          this.isLoading = false
                        },
                        error: (error) => {
                          this.notification.create("error", `${error.message}`, '');
                        }
                      })
                    }
                    else {
                      this.jobService.getJobById(this.stateGet, this.modalItemAddorUpdate.itemData.idManage).subscribe({
                        next: (v) => {
                          if (v.status === false) {
                            this.notification.create("error", `${v.message}`, '');
                          }
                          this.listData = v.content.list
                          this.totalRecord = v.content.totalRecord
                          this.stateGet.currentPage = v.content.currentPage
                          this.isLoading = false
                        },
                        error: (error) => {
                          this.notification.create("error", `${error.message}`, '');
                        }
                      })
                    }
                  },
                  error: (e) => {
                    this.notification.create(
                      "error", `${e.message}`, '');
                    this.isLoading = false
                  }
                })
              }
            },
            error: (error) => {
              this.notification.create("error", `${error.message}`, '');
            }
          })
        this.modalItemAddorUpdate.itemData = {}
      }
    }
    else {
      this.onInput()
    }
  }

  handleOpenModalDelete(data: IJob) {
    this.modalDeleteItem = data
    this.modalDeleteItem.idManage = data.idManage
    this.jobService.checkJob(data.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
          this.modalJob.modalDelete = false;
          this.jobService.refreshListData(this.stateGet).subscribe({
            next: (v) => {
              if (v.status === false) {
                this.notification.create("warning", `${v.message}`, '');
              }
              this.listData = v.content.list
              this.totalRecord = v.content.totalRecord
              this.stateGet.currentPage = v.content.currentPage
              this.isLoading = false
            },
            error: (e) => {
              this.notification.create(
                "error", `${e.message}`, '');
              this.isLoading = false
            }
          })
        }
        else {
          this.modalJob.modalDelete = true;
          this.modalDeleteItem = data
          this.nameJob = data.nameJob;
          this.item.idJob = data.idJob
        }
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
      }
    })
  }

  handleSubmitDelete(): void {
    this.isLoading = true;
    this.jobService.checkJob(this.modalDeleteItem.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
          this.handleCloseModalDelete();
          this.jobService.checkJob(this.modalDeleteItem.idManage)
            .subscribe({
              next: (v) => {
                if (v.status === false) {
                  this.notification.create('warning', `${v.message}`, '');
                  this.handleCloseModalDelete();
                  this.closeModal.emit();
                  this.jobService.refreshListData(this.stateGet)
                    .subscribe({
                      next: (v) => {
                        if (v.status === false) {
                          this.notification.create('warning', `${v.message}`, '');
                        }
                        this.isLoading = false;
                        this.listData = v.content.list;
                        this.totalRecord = v.content.totalRecord;
                      },
                      error: (e) => {
                        this.notification.create('error', `${e.message}`, '');
                        this.isLoading = false;
                      },
                    });
                }
                else {
                  this.jobService.getJobById(this.stateGet, this.modalDeleteItem.idManage)
                    .subscribe({
                      next: (v) => {
                        if (v.status === false) {
                          this.notification.create('warning', `${v.message}`, '');
                          this.handleCloseModalDelete()
                          this.closeModal.emit();
                          this.jobService.refreshListData(this.stateGet)
                            .subscribe({
                              next: (v) => {
                                if (v.status === false) {
                                  this.notification.create('warning', `${v.message}`, '');
                                }
                                this.isLoading = false;
                                this.listData = v.content.list;
                                this.totalRecord = v.content.totalRecord;
                              },
                              error: (e) => {
                                this.notification.create('error', `${e.message}`, '');
                                this.isLoading = false;
                              },
                            });
                        }
                        else {
                          this.handleCloseModalDelete();
                          this.listData = v.content.list;
                          this.totalRecord = v.content.totalRecord;
                          this.isLoading = false;

                        }
                      },
                      error: (e) => {
                        this.notification.create('error', `${e.message}`, '');
                        this.isLoading = false;
                      },
                    });
                }

              },
              error: (e) => {
                this.notification.create('error', `${e.message}`, '');
                this.isLoading = false;
                this.isVisible = false;
              },
            });
        }
        else {
          this.jobService.delete(this.modalDeleteItem.idJob, this.stateGet.currentPage, this.limit).subscribe({
            next: (v) => {
              if (v.status == false) {
                this.notification.create('error', `${v.message}`, '');
                this.modalJob.modalDelete = false;
              }
              else {
                this.notification.create('success', `${v.message}`, '');
                this.modalJob.modalDelete = false;
              }
              this.jobService.getJobById(this.stateGet, this.modalDeleteItem.idManage).subscribe({
                next: (v) => {
                  if (v.status === false) {
                    this.notification.create("warning", `${v.message}`, '');
                    this.closeModal.emit()
                    this.jobService.refreshListData(this.stateGet).subscribe({
                      next: (v) => {
                        if (v.status === false) {
                          this.notification.create("warning", `${v.message}`, '');
                        }
                        this.isLoading = false;
                        this.listData = v.content.list;
                        this.totalRecord = v.content.totalRecord;
                        this.stateGet.currentPage = v.content.currentPage;
                        this.modalJob.modalDelete = false;
                      },
                      error: (e) => {
                        this.notification.create(
                          "error", `${e.message}`, '');
                        this.isLoading = false
                      }
                    })
                  }
                  this.isLoading = false;
                  this.listData = v.content.list;
                  this.totalRecord = v.content.totalRecord;
                  this.stateGet.currentPage = v.content.currentPage;
                  this.modalJob.modalDelete = false;
                },
                error: (e) => {
                  this.notification.create(
                    "error", `${e.message}`, '');
                  this.isLoading = false
                }
              })
            },
            error: (e) => {
              this.notification.create("error", `${e.message}`, '');
            },
          });

        }
      },
      error: (e) => {
        this.notification.create(
          "error", `${e.message}`, '');
      }
    })
  }
  handleCloseModalDelete() {
    this.modalDeleteItem = {};
    this.modalJob.modalDelete = false;
  }

  handleCloseModalJob() {
    this.breadcrumbItems = [];
    this.closeModal.emit();
  }

  handleCloseModalAddorUpdate(): void {
    this.modalJob.modalAddorUpdate = false;
  }

  breadcrumbItems: IJob[] = [];

  goBack() {
    if (this.breadcrumbItems.length === 1) {
      this.breadcrumbItems.pop();
      this.closeModal.emit();
    }
    else {
      this.handleGetById(this.breadcrumbItems[this.breadcrumbItems.length - 2])
      this.breadcrumbItems.pop();
    }
  }
  removeBreadcrumbItemsAfterIndex(index: number) {
    console.log("this.breadcrumbItems : ",this.breadcrumbItems[index]);

    this.modalJobItem =  this.breadcrumbItems[index]

    if (index == (this.breadcrumbItems.length - 1)) {
      this.handleGetById(this.modalJobItem)
    }
    else {
      this.handleGetById(this.modalJobItem)
      this.breadcrumbItems = this.breadcrumbItems.slice(0, index + 1);
    }
  }
  inputValue() {
    this.stateGet.textSearch = "";
    this.searchJob.emit(this.stateGet);
  }

  handleGetById(data: IJob) {
    this.stateGet = {
      textSearch: '',
      sortType: 'desc',
      sortData: 'updatedAt',
      currentPage: 1
    }
    this.modalJobItem = data
    this.isLoading = true

    const isDuplicate = this.breadcrumbItems.some(crumb => crumb.idJob === data.idJob);
    if (!isDuplicate) {
      this.breadcrumbItems.push(data);
    }

    this.jobService.getJobById(this.stateGet, this.modalJobItem.idJob).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create("warning", `${v.message}`, '');
        }
        if (this.modalJobItem.idJob === data.idJob) {
        this.listData = v.content.list
        this.totalRecord = v.content.totalRecord
        this.stateGet.currentPage = v.content.currentPage
        }
        this.isLoading = false
      },
      error: (e) => {
        this.notification.create("error", `${e.message}`, '');
        this.isLoading = false
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['item'] && changes['item'].currentValue.idJob > 0) {
      this.breadcrumbItems.push(this.item)
      this.jobService.getJobById(this.stateGet, this.item.idJob).subscribe({
        next: (v) => {
          if (v.status === false) {
            this.notification.create("warning", `${v.message}`, '');
          }
          this.listData = v.content.list
          this.totalRecord = v.content.totalRecord
          this.stateGet.currentPage = v.content.currentPage
          this.isLoading = false
        },
        error: (e) => {
          this.notification.create("error", `${e.message}`, '');
          this.isLoading = false
        }
      })
    }
  }

  handlePriorityFormat(priority: string) {
    return priorityFormat(priority)
  }

  onInput() {
    this.showValidate = this.nameJob.trim() === '';
  }

  handleResetState() {
    this.idManage = 0;
    this.nameJob = '';
    this.priority = 'ONE';
    this.idStatus = 1;
  }

  handleSearchModalJob() {
    this.stateGet.currentPage = 1;
    this.jobService.getJobById(this.stateGet, this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this. isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handleSortData(e: number) {
    const data = sortCaseFilter(e)
    if (typeof (data) === 'number') {
      return
    }
    this.stateGet.sortType = data.sortType
    this.stateGet.sortData = data.sortData
    this.stateGet.currentPage = 1
    this.jobService.getJobById(this.stateGet, this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    }
    )
  }

  handlePaginationModalJob(event: number) {
    this.isLoading = true;
    this.stateGet.currentPage = event;
    this.jobService.getJobById(this.stateGet, this.breadcrumbItems[this.breadcrumbItems.length - 1].idJob).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (e) => {
        this.notification.create('error', `${e.message}`, '');
        this.isLoading = false;
      },
    });
  }
  handleReSelectSortType(value: number) {
    this.sortItem = value
  }
}
