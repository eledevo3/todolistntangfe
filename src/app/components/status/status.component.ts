import { Component, Input, EventEmitter } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { IStateGetStatus, IStatus } from '../../Interface/IStatus';
import { sortCase, sortCaseFilter } from '../../utils/jobSort';
import { limit } from '../../constantCommon/constantCommon';
import { StatusService } from '../../Service/statusService/status.service';
@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrl: './status.component.css'
})
export class StatusComponent {
  constructor(
    private statusService: StatusService,
    private notification: NzNotificationService
  ) { }

  itemStatus: {
    listData: IStatus[], itemStatus: IStatus, index: number | undefined
  } = {
      listData: [],
      itemStatus: {
        idStatus: 0,
        nameStatus: '',
        description: '',
        default: false,
      },
      index: 0
    }

  modalStatus = {
    modalAddorUpdate: false,
    modalDelete: false
  }

  stateGetStatus = {
    textSearch: '',
    sortType: 'ASC',
    sortData: 'id',
    currentPage: 1,
  };

  listData: IStatus[] = [];
  isSearch: boolean = false;
  isLoading = false;

  currentPage = 1
  calcu = (this.currentPage - 1) * 5 + 1
  totalRecord: number = 0;
  sortCase = sortCase;
  sortItem: number = 1;
  limit = limit;

  modalItemAddorUpdate = {
    listData: {} as IStatus[],
    itemStatus: {} as IStatus,
    index: 0 as number | undefined,
  }

  showValidate: boolean = false;
  idStatus?: number = 0;
  nameStatus: string = '';
  description: string = '';
  default?: boolean = false;

  stateSortCase = {
    sortType: 'ASC',
    sortData: 'id',
  };

  titleStatus: string = '';

  handleCloseModalDelete() {
    this.modalStatus.modalDelete = false;
  }

  inputValue() {
    this.stateGetStatus.textSearch = ""
  }

  ngOnInit(): void {
    this.handleGet(this.stateGetStatus);
  }

  handleGet(stateStatus: any) {
    this.isLoading = true;
    this.stateGetStatus = stateStatus;
    this.statusService.get(this.stateGetStatus).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handlePagination(event: any): void {
    if (!this.isSearch) {
      this.isLoading = true;
      this.stateGetStatus.currentPage = event;
      this.statusService.get(this.stateGetStatus).subscribe({
        next: (v) => {
          this.listData = v.content.list;
          this.totalRecord = v.content.totalRecord;
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    } else {
      this.isLoading = true;
      this.stateGetStatus.currentPage = event;
      this.statusService.get(this.stateGetStatus).subscribe({
        next: (v) => {
          this.listData = v.content.list;
          this.totalRecord = v.content.totalRecord;
          if (v.status === false) {
            this.notification.create('warning', `${v.message}`, '');
          }
          this.isLoading = false;
        },
        error: (error) => {
          this.notification.create('error', `${error.message}`, '');
          this.isLoading = false;
        },
      });
    }
  }

  handleSearch(event: IStateGetStatus) {
    this.isSearch = true;
    this.stateGetStatus.textSearch = event.textSearch ?? ""
    this.stateGetStatus.currentPage = 1
    this.statusService.get(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handleSelect(event: IStateGetStatus): void {
    this.stateGetStatus.sortType = event.sortType ?? "ASC"
    this.stateGetStatus.sortData = event.sortData ?? "id"
    this.stateGetStatus.currentPage = 1;
    this.statusService.get(event).subscribe({
      next: (v) => {
        this.listData = v.content.list;
        this.totalRecord = v.content.totalRecord;
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.isLoading = false;

      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
        this.isLoading = false;
      },
    });
  }

  handleOpenModalAddorUpdate(itemStatus?: IStatus, index?: number) {
    if (itemStatus) {
      this.modalItemAddorUpdate.index = index;
      this.modalItemAddorUpdate.itemStatus = itemStatus
      this.modalItemAddorUpdate.listData = this.listData
      this.titleStatus = "Sửa trạng thái"
      this.statusService.checkIdStatus(this.modalItemAddorUpdate.itemStatus.idStatus).subscribe({
        next: (v) => {
          if (v.status === true) {
            this.modalStatus.modalAddorUpdate = true
          }
          else {
            this.notification.create('warning', 'Trạng thái không tồn tại', '');
            this.modalStatus.modalAddorUpdate = false;
            this.handleGet(this.stateGetStatus);
          }
        },
        error: (e) => {
          this.notification.create('error', `${e.message}`, '');
          this.modalStatus.modalAddorUpdate = false;
        }
      }
      );

    } else {
      this.modalItemAddorUpdate.itemStatus = {
        idStatus: 0,
        nameStatus: '',
        description: '',
        default: false
      };
      this.modalStatus.modalAddorUpdate = true
      this.titleStatus = "Thêm trạng thái"
    }
  }

  handleSubmitAddOrUpdate(statusObject : Omit<IStatus, "idStatus">): void {
    this.nameStatus = statusObject.nameStatus;
    this.description = statusObject.description;
    if (statusObject.nameStatus) {
      this.isLoading = true;
      if (this.modalItemAddorUpdate.itemStatus.idStatus) {
        this.isLoading = true;
        this.statusService
          .update(this.modalItemAddorUpdate.itemStatus.idStatus, statusObject.nameStatus, statusObject.description)
          .subscribe({
            next: (v) => {
              if (v.status === false) {
                this.notification.create('error', `${v.message}`, '');
                if (v.content.warning == null) {
                } else {
                  this.notification.create(
                    'warning',
                    `${v.content.warning}`,
                    ''
                  );
                }
              } else {
                this.notification.create('success', `${v.message}`, '');
                const updatedListData = [...this.modalItemAddorUpdate.listData]
                const itemStatusUpdate: IStatus = {
                  idStatus: this.modalItemAddorUpdate.itemStatus.idStatus,
                  nameStatus: statusObject.nameStatus,
                  description: statusObject.description,
                  default: this.modalItemAddorUpdate.itemStatus.default,
                }
                if (this.modalItemAddorUpdate.index === undefined) {
                  updatedListData[0] = itemStatusUpdate
                } else {
                  updatedListData[this.modalItemAddorUpdate.index] = itemStatusUpdate
                }
                this.listData = updatedListData
              }
              this.isLoading = false;
              this.handleResetState();
              this.modalStatus.modalAddorUpdate = false;

              this.handleGet(this.stateGetStatus);

            },
            error: (error) => {
              this.notification.create('error', `${error.message}`, '');
              this.isLoading = false;
            },
          });
      } else {
        this.isLoading = true;
        this.statusService.add(statusObject.nameStatus, statusObject.description).subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create('warning', `${v.message}`, '');
            } else {
              this.notification.create('success', `${v.message}`, '');
            } const currentPage = v.content.currentPage;
            this.stateGetStatus.textSearch = ""
            this.stateGetStatus.sortData = "id"
            this.stateGetStatus.sortType = "ASC"
            this.stateGetStatus.currentPage = currentPage
            this.isLoading = false;
            this.handleResetState();
            this.modalStatus.modalAddorUpdate = false;
            this.handleGet(this.stateGetStatus);
          }
        })
      }
    }
  }


  handleCloseModalAddorUpdate() {
    this.handleResetState()
    this.modalStatus.modalAddorUpdate = false;
  }

  handleResetState(): void {
    this.idStatus = 0;
    this.nameStatus = '';
    this.description = '';
    this.default = false;
  }

  handleSubmitDelete(): void {
    this.isLoading = true;
    this.statusService.checkStatus(this.idStatus).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
        }
        this.statusService.delete(this.idStatus, this.currentPage).subscribe({
          next: (v) => {
            if (v.status === false) {
              this.notification.create('error', `${v.message}`, '');
              if (v.content.warning == null) {
              } else {
                this.notification.create(
                  'warning',
                  `${v.content.warning}`,
                  ''
                );
              }
            } else {
              this.notification.create('success', `${v.message}`, '');
          this.modalStatus.modalDelete = false;
          this.handleGet(this.stateGetStatus);
            }
            this.isLoading = false;
          },
          error: (error) => {
            this.notification.create('error', `${error.message}`, '');
            this.isLoading = false;
          },
        });
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
      },
    });
  }

  handleCancel(): void {
    this.handleCloseModalAddorUpdate();
    this.handleResetState();
    this.showValidate = false;
  }

  handleOpenModalDelete(item: IStatus) {
    this.statusService.checkStatus(item.idStatus).subscribe({
      next: (v) => {
        if (v.status === false) {
          this.notification.create('warning', `${v.message}`, '');
          this.modalStatus.modalDelete = false;
          this.handleGet(this.stateGetStatus);
        }
        else {
          this.modalStatus.modalDelete = true;
          this.nameStatus = item.nameStatus
          this.idStatus = item.idStatus
        }
      },
      error: (error) => {
        this.notification.create('error', `${error.message}`, '');
      },
    });
  }
}

