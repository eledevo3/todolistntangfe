import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { IStatus } from '../../../../Interface/IStatus';

@Component({
  selector: 'app-modal-add-or-update-status',
  templateUrl: './modal-add-or-update-status.component.html',
  styleUrl: './modal-add-or-update-status.component.css'
})
export class ModalAddOrUpdateStatusComponent {

  @Input() isVisibleStatus: boolean = false;

  @Input() itemStatus: {
    listData: IStatus[], itemStatus: IStatus, index: number | undefined
  } = {
      listData: [],
      itemStatus: {
        idStatus: 0,
        nameStatus: '',
        description: '',
        default: false,
      },
      index: 0
  }

  @Input() stateStatus: any = {
    textSearch: '',
    sortType: '',
    sortData: '',
    currentPage: 1,
  };

  statusObject : Omit<IStatus, "idStatus">={
    nameStatus: "",
    description: "",
    default: false,
  }

  @Input() titleStatus: string = '';

  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  @Output() AddOrUpdateEvent: EventEmitter<Omit<IStatus, "idStatus">> = new EventEmitter<Omit<IStatus, "idStatus">>();

  isLoading = false;
  showValidate: boolean = false;
  idStatus?: number = 0;
  nameStatus: string = '';
  description: string = '';
  default?: boolean = false;

  onInput() {
    this.showValidate = this.nameStatus.trim() === '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes['itemStatus'] &&
      changes['itemStatus'].currentValue.idStatus > 0
    ) {
      this.idStatus = this.itemStatus.itemStatus.idStatus;
      this.nameStatus = this.itemStatus.itemStatus.nameStatus;
      this.description = this.itemStatus.itemStatus.description;
      this.default = this.itemStatus.itemStatus.default;  
    } else if (this.itemStatus.itemStatus.idStatus > 0) {
      this.idStatus = this.itemStatus.itemStatus.idStatus;
      this.nameStatus = this.itemStatus.itemStatus.nameStatus;
      this.description = this.itemStatus.itemStatus.description;
      this.default = this.itemStatus.itemStatus.default;
    } else {
      this.idStatus = undefined;
      this.nameStatus = '';
      this.description = '';
      this.default = false;
    }
  }

  handleOk(): void{
    this.statusObject.nameStatus = this.nameStatus;
    this.statusObject.description = this.description;
    this.AddOrUpdateEvent.emit(this.statusObject);
  }

  handleCancel(){
    this.closeModal.emit();
  }
}
