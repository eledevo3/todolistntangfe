import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDeleteStatusComponent } from './modal-delete-status.component';

describe('ModalDeleteStatusComponent', () => {
  let component: ModalDeleteStatusComponent;
  let fixture: ComponentFixture<ModalDeleteStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalDeleteStatusComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModalDeleteStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
