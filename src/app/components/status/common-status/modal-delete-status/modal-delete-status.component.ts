import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IStatus } from '../../../../Interface/IStatus';
import { StatusService } from '../../../../Service/statusService/status.service';


@Component({
  selector: 'app-modal-delete-status',
  templateUrl: './modal-delete-status.component.html',
  styleUrl: './modal-delete-status.component.css'
})
export class ModalDeleteStatusComponent {
  constructor(
    private statusService: StatusService,
    private notification: NzNotificationService
  ) {}

  @Output() closeModal: EventEmitter<void> = new EventEmitter<void>();
  @Output() getStatus = new EventEmitter<any>();
  @Output() deleteEvent: EventEmitter<void> = new EventEmitter<void>();
  @Input() isVisibleDeleteStatus: boolean = false;
  @Input() nameStatus: string = ''
  @Input() stateStatus = {
    textSearch: '',
    sortType: 'ASC',
    sortData: 'id',
    currentPage: 1,
  }
  isLoading = false;

  handleOk(): void {
    this.deleteEvent.emit();
  }
  
  handleCancel(): void {
    this.closeModal.emit();
  }
}
