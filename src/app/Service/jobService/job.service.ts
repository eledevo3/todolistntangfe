import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, ReplaySubject, tap } from 'rxjs';
import { IJob,IPayloadJob, IResponse, IStateGetJob } from '../../Interface/IJob';
import { apiMethod, baseUrl, objectApi, limit } from '../../constantCommon/constantCommon';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  constructor(private http : HttpClient) { }
  private response = new ReplaySubject<IResponse>()
  get(): Observable<IResponse> {
    return this.response.asObservable()
  }

  checkJob(idJob: number | undefined): Observable<IResponse>{
    return this.http.get<any>(
      `${baseUrl}${objectApi.job}${apiMethod.checkIdJob}/${idJob}`
    );
  }

  delete(idJob: number,currentPage: number,limit: number): Observable<any>{
    return this.http.delete<any>(
      `${baseUrl}${objectApi.job}${apiMethod.delete}/${idJob}?currentPage=${currentPage}&limit=${limit}`
    )
  }
  add(idManage: number | null = 0, nameJob: string, priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE', idStatus: number | undefined = 0, nameStatus: string | undefined = ""): Observable<any> {
    return this.http.post<any>(
      `${baseUrl}${objectApi.job}${apiMethod.post}?idManage=${idManage}`, {nameJob, priority, idStatus, nameStatus}
    );
  }
  update(idJob: number, nameJob: string, priority: 'ONE' | 'TWO' | 'THREE' | 'FOUR' | 'FIVE', idStatus: number | undefined = 0  , nameStatus: string | undefined ): Observable<any> {
    return this.http.put<any>(
      `${baseUrl}${objectApi.job}${apiMethod.put}/${idJob}`,
      {nameJob, priority, idStatus, nameStatus}
    )
  }

  refreshListData(data: IStateGetJob): Observable<IResponse>{
    return this.http.get<IResponse>(`${baseUrl}${objectApi.job}${apiMethod.getAll}?textSearch=${data.textSearch}&currentPage=${data.currentPage}&sortData=${data.sortData}&sortType=${data.sortType}&limit=${limit}`)
    .pipe(tap(res => {
      this.response.next(res)
    }))
  }

  getJobById(data: IStateGetJob, idJob: number): Observable<IResponse>{
    return this.http.get<IResponse>(`${baseUrl}${objectApi.job}${apiMethod.get}?idJob=${idJob}&textSearch=${data.textSearch}&currentPage=${data.currentPage}&sortData=${data.sortData}&sortType=${data.sortType}&limit=${limit}`)
  }
}
