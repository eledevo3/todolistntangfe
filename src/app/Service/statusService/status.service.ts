import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject, tap } from 'rxjs';
import { IStateGetStatus, IResponse, IStatus } from '../../Interface/IStatus';
import {
  apiMethod,
  baseUrl,
  objectApi,
  limit,
} from '../../constantCommon/constantCommon';

@Injectable({
  providedIn: 'root'
})
export class StatusService {
  constructor(private http: HttpClient) {}
  get(data: IStateGetStatus): Observable<IResponse> {
    return this.http.get<IResponse>(
      `${baseUrl}${objectApi.status}${apiMethod.get}?textSearch=${data.textSearch}&currentPage=${data.currentPage}&limit=${limit}&sortData=${data.sortData}&sortType=${data.sortType}`
    );
  }

  getAll(): Observable<IResponse> {
    return this.http.get<IResponse>(
      `${baseUrl}${objectApi.status}${apiMethod.getAll}`
    );
  }

  add(nameStatus: string,description: string): Observable<any> {
    return this.http.post<any>(
      `${baseUrl}${objectApi.status}${apiMethod.post}?&limit=${limit}`,
      {nameStatus,description}
    );
  }

  delete(idStatus: number | undefined,currentPage: number): Observable<any> {
    return this.http.delete<any>(
      `${baseUrl}${objectApi.status}${apiMethod.delete}/${idStatus}?currentPage=${currentPage}&limit=${limit}`
    );
  }

  checkIdStatus(idStatus: number): Observable<any>{
    return this.http.get
    <any>(`${baseUrl}${objectApi.status}${apiMethod.checkIdStatus}/${idStatus}`)
  }
  
  update(idStatus: number | undefined,nameStatus: string ,description: string): Observable<any> {
    return this.http.put<any>(
      `${baseUrl}${objectApi.status}${apiMethod.put}/${idStatus}`,
      {nameStatus,description}
    )
  }
  
  checkStatus(idStatus: number | undefined): Observable<IResponse>{
    return this.http.get<any>(
      `${baseUrl}${objectApi.status}${apiMethod.checkIdStatus}/${idStatus}`
    );
  }
}

